
const FAST1 =  PIXI.Sprite.from('../assets/FAST1.png');

app.stage.addChild(FAST1);

FAST1.anchor.set(0.5);
FAST1.x=window.innerWidth/2.65;
FAST1.y=window.innerHeight/4;

FAST1.width = window.innerWidth/5.5;
FAST1.height = window.innerHeight/10;


// loader.add('tileset', '../assets/wave.json')
// .load(setup);

// function setup(loader, resources){
//     const textures = [];
//     for(let i=1; i<13 ;i++) {
//         const texture = PIXI.Texture.from(`wave${i}.png`);
//         textures.push(texture);
//     }
//     const wave = new PIXI.AnimatedSprite(textures);
//     wave.position.set(800,300);
//     wave.scale.set(2.2);
//     app.stage.addChild(wave);
//     wave.play();
//     wave.animationSpeed = 0.1;
// }




const Player1 =  PIXI.Sprite.from('../assets/Player.png');

app.stage.addChild(Player1);

Player1.anchor.set(0.5);
Player1.x=window.innerWidth/3;
Player1.y=window.innerHeight/5;

Player1.width = window.innerWidth/20;
Player1.height = window.innerHeight/4.25;

function addCar1(){
    app.stage.addChild(FAST1);
}

function removeCar1(){
    app.stage.removeChild(FAST1);
}

function addPlayer1(){
    app.stage.addChild(Player1);
}

function removePlayer1(){
    app.stage.removeChild(Player1);
}

function updateState1(state){
    switch(state) {
        case 1 : // if (x === 'value1')
            addPlayer1();
            removeCar1();
        break;
        
        case 2 : // if (x === 'value2')
            addCar1();
            addPlayer1();
        break;
        
        default:
            removeCar1();
            removePlayer1();
        break;
        }
}