
const FAST4 =  PIXI.Sprite.from('../assets/FAST4.png');

app.stage.addChild(FAST4);

FAST4.anchor.set(0.5);
FAST4.x=window.innerWidth/1.585;
FAST4.y=window.innerHeight/4;

FAST4.width = window.innerWidth/5.5;
FAST4.height = window.innerHeight/10;


const Player4 =  PIXI.Sprite.from('../assets/Player4.png');

app.stage.addChild(Player4);

Player4.anchor.set(0.5);
Player4.x=window.innerWidth/1.48;
Player4.y=window.innerHeight/5;

Player4.width = window.innerWidth/20;
Player4.height = window.innerHeight/4.25;


function addCar4(){
    app.stage.addChild(FAST4);
}

function removeCar4(){
    app.stage.removeChild(FAST4);
}

function addPlayer4(){
    app.stage.addChild(Player4);
}

function removePlayer4(){
    app.stage.removeChild(Player4);
}

function updateState4(state){
    switch(state) {
        case 1 : // if (x === 'value1')
            addPlayer4();
            removeCar4();
        break;
        
        case 2 : // if (x === 'value2')
            addCar4();
            addPlayer4();
        break;
        
        default:
            removeCar4();
            removePlayer4();
        break;
        }
}

updateState4(2);