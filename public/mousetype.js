const defaultIcon = "url('../assets/disk2.png') 2 2, auto";
const hoverIcon = "url('../assets/disk1.png') 2 2, auto";


app.renderer.plugins.interaction.cursorStyles.default = defaultIcon;
app.renderer.plugins.interaction.cursorStyles.hover = hoverIcon;