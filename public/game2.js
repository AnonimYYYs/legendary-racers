


const textureButton = PIXI.Texture.from('../assets/button1n.png');
const textureButtonDown = PIXI.Texture.from('../assets/button2.png');

const buttons = [];

for (let i = 0; i < 1; i++) {
    const button = new PIXI.Sprite(textureButton);
    button.cursor = 'hover';

    button.anchor.set(0.5);
    button.x=window.innerWidth/2;
    button.y=window.innerHeight/2;
    
    button.width = window.innerWidth/6;
    button.height = window.innerHeight/6;

    // make the button interactive...
    button.interactive = true;
    button.buttonMode = true;

    button
        .on('pointerdown', onButtonDown)
       

    // add it to the stage
    app.stage.addChild(button);

    // add button to array
    buttons.push(button);
}

// set some silly values...
// buttons[0].scale.set(1.2);

function onButtonDown() {
    JoinHitHandler();
    this.isdown = true;
    this.texture = textureButtonDown;
    this.alpha = 1;
}


