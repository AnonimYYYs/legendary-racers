
const FAST2 =  PIXI.Sprite.from('../assets/FAST2.png');

app.stage.addChild(FAST2);

FAST2.anchor.set(0.5);
FAST2.x=window.innerWidth/2.65;
FAST2.y=window.innerHeight/1.82;

FAST2.width = window.innerWidth/5.5;
FAST2.height = window.innerHeight/10;


const Player2 =  PIXI.Sprite.from('../assets/Player2.png');

app.stage.addChild(Player2);

Player2.anchor.set(0.5);
Player2.x=window.innerWidth/3;
Player2.y=window.innerHeight/2;

Player2.width = window.innerWidth/20;
Player2.height = window.innerHeight/4.25;


function addCar2(){
    app.stage.addChild(FAST2);
}

function removeCar2(){
    app.stage.removeChild(FAST2);
}

function addPlayer2(){
    app.stage.addChild(Player2);
}

function removePlayer2(){
    app.stage.removeChild(Player2);
}

function updateState2(state){
    switch(state) {
        case 1 : // if (x === 'value1')
            addPlayer2();
            removeCar2();
        break;
        
        case 2 : // if (x === 'value2')
            addCar2();
            addPlayer2();
        break;
        
        default:
            removeCar2();
            removePlayer2();
        break;
        }
}

updateState2(2);