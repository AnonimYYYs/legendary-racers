
const FAST3 = PIXI.Sprite.from('../assets/FAST3.png');

FAST3.anchor.set(0.5);
FAST3.x=window.innerWidth/1.585;
FAST3.y=window.innerHeight/1.82;

FAST3.width = window.innerWidth/5.5;
FAST3.height = window.innerHeight/10;



const Player3 = PIXI.Sprite.from('../assets/Player3.png');

Player3.anchor.set(0.5);
Player3.x=window.innerWidth/1.48;
Player3.y=window.innerHeight/2;

Player3.width = window.innerWidth/20;
Player3.height = window.innerHeight/4.25;


function addCar3(){
    app.stage.addChild(FAST3);
}

function removeCar3(){
    app.stage.removeChild(FAST3);
}

function addPlayer3(){
    app.stage.addChild(Player3);
}

function removePlayer3(){
    app.stage.removeChild(Player3);
}

function updateState3(state){
    switch(state) {
        case 1 : // if (x === 'value1')
            addPlayer3();
            removeCar3();
        break;
        
        case 2 : // if (x === 'value2')
            addCar3();
            addPlayer3();
        break;
        
        default:
            removeCar3();
            removePlayer3();
        break;
        }
}

updateState3(2);
