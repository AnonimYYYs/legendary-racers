
const app = new PIXI.Application({
    width: 500,
    height: 500,
    transparent: false,
    antialis: false,
    // resolution: window.devicePixelRatio|| 1
});


  document.querySelector('.container').appendChild(app.view);
  const videoRef = document.querySelector("video");
  
    const texture = PIXI.Texture.from(videoRef);
    const videoSprite = new PIXI.Sprite(texture);
   
    app.stage.addChild(videoSprite);
    app.renderer.resize(window.innerWidth, window.innerHeight);

    videoSprite.width = app.screen.width;
    videoSprite.height = app.screen.height;

    const background = PIXI.Sprite.from('../assets/Ton.png');
    background.width = app.screen.width;
    background.height = app.screen.height;
    app.stage.addChild(background);
    
    background.phase = 0;

  videoRef.oncanplay = start;





