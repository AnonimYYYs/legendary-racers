var express = require('express');
var app = express();
var server = app.listen(3000);
app.use(express.static('public'));
var curPlayerNumber = 1;

var socket = require('socket.io');
var io = socket(server);
io.sockets.on('connection', newConnection);

var state = "Waiting";		//possible setver states "Waiting", "InGame"

const players = new Map();	//possible states 'not ready', 

console.log("server running");


// game definitions
const FIELD_SIZE = 1000;
const SIDE_A_BIAS = FIELD_SIZE / 2;
const SIDE_B_BIAS = FIELD_SIZE / 20;
const SPEED = 3;
const MAX_ANGLE = Math.PI / 180;	// Math.PI / 180 = 1 degree
var Victor = require('victor');

var g = new Game();

class Player {
    constructor(id) {
        this.id = id;
        this.isFailed = false;
        switch (this.id) {
            case 1: {
                this.pos = new Victor(SIDE_A_BIAS, SIDE_B_BIAS);
                this.dir = new Victor(0, SPEED);
                break;
            }
            case 2: {
                this.pos = new Victor(SIDE_A_BIAS, FIELD_SIZE - SIDE_B_BIAS);
                this.dir = new Victor(0, -SPEED);
                break;
            }
            case 3: {
                this.pos = new Victor(FIELD_SIZE - SIDE_B_BIAS, SIDE_A_BIAS);
                this.dir = new Victor(-SPEED, 0);
                break;
            }
            case 4: {
                this.pos = new Victor(SIDE_B_BIAS, SIDE_A_BIAS);
                this.dir = new Victor(SPEED, 0);
                break;
            }
            default: {
                this.pos = new Victor(0, 0);
                this.dir = new Victor(0, 0);
                break;
            }
        }
        this.mouse = new Victor(SIDE_A_BIAS, SIDE_A_BIAS);
        this.oldPos = Victor.fromObject(this.pos);
    }
    updateMouse(x, y) {
        this.mouse = new Victor(x, y);
    }
    updatePos() {
        if (!(this.isFailed)) {
            this.oldPos.copy(this.pos);
            this.pos.add(this.dir);
        }
    }
    updateDir() {
        if (!(this.isFailed)) {
            let newDir = Victor.fromObject(this.mouse);
            newDir.subtract(this.pos);
            let newAng = newDir.angle() - this.dir.angle();
            if (Math.abs(newAng) > Math.PI) {
                newAng -= Math.PI * 2;
            }
            let sign = 1;
            if (newAng < 0) {
                sign = -1;
            }
            newAng = Math.abs(newAng);
            if (newAng > MAX_ANGLE) {
                newAng = MAX_ANGLE;
            }
            ;
            this.dir.rotate(sign * newAng);
        }
    }
    setFail() {
        this.isFailed = true;
    }
}

// one game instance
// create this instance
class Game {
    constructor() {
        // game id
        this.failedAmount = 0;
        // players
        this.players = [];
        this.playersAmount = 0;
        this.field = [];
        for (let i = 0; i < FIELD_SIZE; i++) {
            this.field[i] = [];
            for (let j = 0; j < FIELD_SIZE; j++) {
                this.field[i][j] = 0;
            }
        }
    }
	
	// function to add new player, work only if less then 4 already exist
	// id is from 1 to 4
    addPlayer() {
        if (this.players.length >= 4) {
            return false;
        }
        else {
            let newId = this.players.length + 1;
            this.players.push(new Player(newId));
            let coord = this.players[this.playersAmount].pos;
            this.field[Math.floor(coord.x)][Math.floor(coord.y)] = newId;
            this.playersAmount++;
        }
        return true;
    }
	
	// set new mouse position for player with id
    setMouse(id, x, y) {
        if (id >= 1 && id <= this.playersAmount) {
            this.players[id - 1].updateMouse(x, y);
        }
    }
	
	// call this function to make one game-step
    update() {
        for (let idx = 0; idx < this.players.length; idx++) {
            if (!(this.players[idx].isFailed)) {
                this.players[idx].updatePos();
                this.players[idx].updateDir();
 
                let step = this.players[idx].pos.clone().subtract(this.players[idx].oldPos).divide(new Victor(SPEED * 1000, SPEED * 1000));
                let stepPos = this.players[idx].oldPos.clone();
                for (let i = 0; i < SPEED; i++) {
                    if (this.field[Math.floor(stepPos.x)][Math.floor(stepPos.y)] == 0 ||
                        this.field[Math.floor(stepPos.x)][Math.floor(stepPos.y)] == this.players[idx].id) {
                        this.field[Math.floor(stepPos.x)][Math.floor(stepPos.y)] = this.players[idx].id;
                    }
                    else {
                        this.players[idx].setFail();
                        this.failedAmount++;
                        break;
                    }
                    stepPos.add(step);
                }
            }
        }
    }
	
	// check if game ended
    isEnd() {
        if ((this.failedAmount + 1) >= this.playersAmount) {
            return true;
        }
        return false;
    }
}


function newConnection(socket) {

	var id = socket.id;

	var playerNumber = 0;

	socket.on("set location", (data) => {
		if (data != 'menu')
			players.set(id, data);
		if (data == 'lobby') {
			console.log('joined lobby with id ' + id)
			players.set(id, 'not ready');
			OnLobbyPlayerChange(players);
		}
	})

	socket.on('JoinHit', () => {
		var reply;
		console.log('Join Hit');
		if (state == "Waiting") {
			if (players.size < 4) {
				reply = 'may join';
				console.log(reply);
			}
			else {
				reply = 'Room is full';
				console.log(reply);
			}
		}
		else {
			if (state == "InGame") {
				reply = 'Game is in progress';
				console.log(reply);
			}
		}
		io.to(id).emit('join reply', reply);
		console.log("\n");
	});

	socket.on("ready", (state) => {
		//console.log(Object.keys(io.sockets));
		//console.log(io.sockets.adapter.rooms);
		if(state==true)
			players.set(id, 'ready');
		else
			players.set(id, 'not ready');
		OnLobbyPlayerChange(players);
	})

	socket.on("leave", () => {
		players.delete(id);
		OnLobbyPlayerChange(players);
	})


	socket.on("disconnect", () => {
		var state = players.get(socket.id);
		players.delete(id);
		switch (state) {
			case 'ready':
			case 'not ready':
				console.log('Player left from lobby');
				OnLobbyPlayerChange(players);
				break;
			case 'InGame':
				console.log("Player left from game");
				console.log("Now " + players.size + " players in lobby")
				break;
		}
	})

	function OnLobbyPlayerChange(players) {
		if(CheckGameReady()){
			state == "InGame";
			StartGame()
		} else {
			PrintLobbyPlayers();
			BroadcastToLobbyPlayers(players);
		}
    }
	

	function BroadcastToLobbyPlayers(players)
	{
			var curPlayer = 1;
				players.forEach((valueOuter, keyOuter) => {
					if (valueOuter == 'ready' || valueOuter == 'not ready') {
						players.forEach((valueInner, keyInner) => {
							io.to(keyInner).emit('PlayerLobbyStateSet', curPlayer + valueOuter);
			console.log(keyInner + ' recieved' + 'PlayerLobbyStateSet', curPlayer + valueOuter);
			})
		} 
			curPlayer++;
		});


		for (; curPlayer < 5; curPlayer++)
		{
			players.forEach((valueInner, keyInner) => {
				io.to(keyInner).emit('PlayerLobbyStateSet', curPlayer + 'empty');
				console.log(keyInner + ' recieved' + 'PlayerLobbyStateSet', curPlayer + 'empty');
			})
		}
	}

	function PrintLobbyPlayers() {
		var ready = 0;
		var notReady = 0;
		players.forEach((value, key) => {
			if (value == 'ready') {
				ready++;
			}
			else {
				notReady++;
			}
		})	
		console.log('there are ' + ready + ' ready players in lobby')
    }

	function CheckGameReady() {
		if(players.size < 2) {
			return false;
		}
		players.forEach((value, key) => {
			if (value != 'ready') {
				return false;
			}
		})	
		return true;
    }

	function StartGame(players) {
		g = new Game();
		let s = 1;
		players.forEach(element => {
			element.value = s;
			s++;
			g.addPlayer();
		})
		
		for(;!g.isEnd();)
		{
			g.update();
		}
	}
}
